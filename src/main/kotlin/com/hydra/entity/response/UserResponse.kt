package com.hydra.entity.response

import com.hydra.data.objects.Appointment
import com.hydra.data.objects.Status
import com.hydra.data.objects.Type

data class UserResponse(val id: String = "",
                        val name: String? = "",
                        val type: Type = Type.ASSISTANT,
                        val status: Status = Status.LOGIN,
                        val group: String = "",
                        val appointment: List<Appointment> = emptyList())