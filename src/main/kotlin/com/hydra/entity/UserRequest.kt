package com.hydra.entity

import com.hydra.data.objects.*

data class UserRequest (
        val id: String = "",
        val name: String = "",
        val password: String = "",
        val type: Type = Type.ASSISTANT,
        val status: Status = Status.LOGIN,
        val patients: List<String> = emptyList(),
        val medicare: List<String> = emptyList(),
        val doctors: List<String> = emptyList()
)