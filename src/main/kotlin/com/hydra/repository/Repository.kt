package com.hydra.repository

import com.hydra.data.objects.*
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import java.time.Instant
import java.util.*

@Repository
interface UserRepository : MongoRepository<User, String> {
    fun findByIdAndPassword(id: String, password: String): Optional<User>
}

@Repository
interface DoctorRepository: ReactiveMongoRepository<Doctor, String>

@Repository
interface MedicareRepository: ReactiveMongoRepository<Medicare, String>

@Repository
interface PatientRepository: ReactiveMongoRepository<Patient, String>

@Repository
interface AppointmentRepository: MongoRepository<Appointment, String> {
    fun findByAppointmentWith(appointmentWith: String): List<Appointment>
    fun deleteByAppointmentWith(appointmentWith: String)
}