package com.hydra.controller

import com.hydra.data.objects.Patient
import com.hydra.service.PatientService
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/patient")
class PatientController(private val patientService: PatientService) {

    @PostMapping(consumes = [ MediaType.APPLICATION_JSON_VALUE ] , produces = [ MediaType.APPLICATION_JSON_VALUE ])
    fun  insertPatient(@RequestBody patient: Patient)
            : ResponseEntity<Mono<Patient>> = ResponseEntity.accepted().body(patientService.insertPatient(patient))

    @PutMapping(consumes = [ MediaType.APPLICATION_JSON_VALUE ] , produces = [ MediaType.APPLICATION_JSON_VALUE ])
    fun  updatePatient(@RequestBody patient: Patient)
            : ResponseEntity<Mono<Patient>> = ResponseEntity.ok().body(patientService.updatePatient(patient))

    @GetMapping(produces = [ MediaType.APPLICATION_JSON_VALUE ] , path = ["/{id}"])
    fun  fetchPatient(@PathVariable id: String)
            : ResponseEntity<Mono<Patient>> = ResponseEntity.accepted().body(patientService.fetchPatientById(id))
}