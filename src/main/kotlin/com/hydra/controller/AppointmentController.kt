package com.hydra.controller


import com.hydra.data.objects.Appointment
import com.hydra.service.AppointmentService
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/appointment")
class AppointmentController(private val appointmentService: AppointmentService) {

    @PostMapping(consumes = [ MediaType.APPLICATION_JSON_VALUE ] , produces = [ MediaType.APPLICATION_JSON_VALUE ])
    fun insertAppointment(appointment: Appointment) : ResponseEntity<Appointment> =
            ResponseEntity.accepted().body(appointmentService.insertAppointment(appointment))


    @GetMapping( produces = [ MediaType.APPLICATION_JSON_VALUE ] , path = [ "/{appointmentWith}" ])
    fun fetchAppointmentBy(@PathVariable appointmentWith: String): ResponseEntity<List<Appointment>> =
            ResponseEntity.ok(appointmentService.findAppointmentByDoctor(appointmentWith))
}