package com.hydra.controller

import com.hydra.entity.UserRequest
import com.hydra.data.objects.User
import com.hydra.entity.response.UserResponse
import com.hydra.service.UserService
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/user")
class UserController(val userService: UserService) {

    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE],produces = [MediaType.APPLICATION_JSON_VALUE])
    fun insertUser(@RequestBody userRequest: UserRequest):ResponseEntity<Mono<UserResponse>> =
            ResponseEntity.status(HttpStatus.CREATED).body(userService.insertUser(userRequest))

    @PutMapping(consumes = [MediaType.APPLICATION_JSON_VALUE],produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateUser(@RequestBody userRequest: UserRequest) : ResponseEntity<Mono<UserResponse>> =
            ResponseEntity.status(HttpStatus.ACCEPTED).body(userService.updateUser(userRequest))


    @GetMapping(produces = [MediaType.APPLICATION_JSON_VALUE] , path = ["/{id}"])
    fun fetchUserById(@PathVariable id: String): ResponseEntity<Mono<UserResponse>>
            = ResponseEntity.ok(userService.fetchUserById(id))


    @PostMapping(produces = [MediaType.APPLICATION_JSON_VALUE] , path = ["/validateUser"])
    fun validateUser(@RequestBody userRequest: UserRequest): ResponseEntity<Mono<Boolean>> {
        return ResponseEntity.ok(userService.validateUser(userRequest))
    }

    @DeleteMapping(path = ["/{id}"])
    fun deleteUser(@PathVariable id:String): ResponseEntity<Mono<String>> {
        return ResponseEntity.ok(userService.deleteUser(id))
    }

}