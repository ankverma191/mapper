package com.hydra.service

import com.hydra.data.objects.Appointment
import com.hydra.repository.AppointmentRepository
import org.springframework.stereotype.Service
import java.time.Instant

@Service
class AppointmentService(private val appointmentRepository: AppointmentRepository) {

    fun insertAppointment(appointment: Appointment) : Appointment = appointmentRepository.insert(appointment)
    fun findAppointmentByDoctor(doctorId: String): List<Appointment>  {
        return appointmentRepository.findByAppointmentWith(doctorId)
    }

}