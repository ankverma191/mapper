package com.hydra.service

import com.hydra.data.objects.*
import com.hydra.entity.UserRequest
import com.hydra.entity.response.UserResponse
import com.hydra.repository.*
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.lang.RuntimeException
import java.lang.UnsupportedOperationException
import java.time.Instant

@Service
class UserService(val userRepository: UserRepository,
                  val doctorRepository: DoctorRepository,
                  val patientRepository: PatientRepository,
                  val medicareRepository: MedicareRepository,
                  val appointmentRepository: AppointmentRepository) {


    fun insertUser(userRequest: UserRequest): Mono<UserResponse>? {
        userRepository.insert(User(id = userRequest.id, name = userRequest.name,
                password = userRequest.password,
                date = Instant.now(),
                group = userRequest.type.name + "_GROUP",
                status = userRequest.status,
                type = userRequest.type))

        return updateUser(userRequest)
    }

    fun updateUser(userRequest: UserRequest): Mono<UserResponse>? {
        return when(userRequest.type) {
            Type.DOCTOR ->  saveDoctor(Doctor(doctor_id = userRequest.id, doctor_medicare = userRequest.medicare, doctor_name = userRequest.name, doctor_patients = userRequest.patients))
            Type.MEDICARE ->  saveMedicare(Medicare(medicare_id = userRequest.id, medicare_doctors = userRequest.doctors, medicare_name = userRequest.name, medicare_patients = userRequest.patients))
            Type.PATIENT ->  savePatient(Patient(patientId = userRequest.id, patientDoctors = userRequest.doctors, patientName = userRequest.name, patientMedicare = userRequest.medicare, patientType = PatientType.NORMAL))
            else -> throw UnsupportedOperationException("type [ " + userRequest.type + " ] is not supported")
        }
    }

    fun fetchUserById(id: String): Mono<UserResponse> {
        val user = userRepository.findById(id).orElse(null)
        if (user!=null) {
            val appList = appointmentRepository.findByAppointmentWith(user.id)
            return when (user.type) {
                Type.PATIENT -> patientRepository.findById(id).map {
                    UserResponse(id = it.patientId, group = user.group, status = user.status, type = user.type, appointment = appList, name = it.patientName)
                }.toMono()

                Type.MEDICARE -> medicareRepository.findById(id).map {
                    UserResponse(id = it.medicare_id, group = user.group, status = user.status, type = user.type, appointment = appList, name = it.medicare_name)
                }.toMono()

                Type.DOCTOR -> doctorRepository.findById(id).map {
                    UserResponse(id = it.doctor_id, group = user.group, status = user.status, type = user.type, appointment = appList, name = it.doctor_name)
                }.toMono()
                else -> throw UnsupportedOperationException("type [ " + user.type + " ] is not supported")
            }
        }
        return Mono.just(UserResponse())
    }


    fun deleteUser(id: String): Mono<String>  {
        val user = userRepository.findById(id)
        return if (user.isPresent) {
            userRepository.delete(user.get())
            appointmentRepository.deleteByAppointmentWith(id)
            when(user.get().type) {
                Type.DOCTOR -> doctorRepository.deleteById(id).map { "user ${user.get().id} is deleted successfully" }.toMono()
                Type.MEDICARE -> medicareRepository.deleteById(id).map { "user ${user.get().id} is deleted successfully" }.toMono()
                Type.PATIENT -> patientRepository.deleteById(id).map { "user ${user.get().id} is deleted successfully" }.toMono()
                else -> throw UnsupportedOperationException("type [ " + user.get().type + " ] is not supported")
            }
        } else {
            Mono.just("user $id is not found")
        }
    }

    
    fun validateUser(userRequest: UserRequest): Mono<Boolean> {
        return Mono.just(userRepository.findByIdAndPassword(userRequest.id, userRequest.password).isPresent)
    }


    private fun saveDoctor(doctor: Doctor): Mono<UserResponse> {
        val appList = appointmentRepository.findByAppointmentWith(doctor.doctor_id)
        return doctorRepository.save(doctor).map{
            UserResponse(id = doctor.doctor_id , name = doctor.doctor_name, type = Type.DOCTOR, status = Status.LOGIN, group = Type.DOCTOR.name + "_GROUP",appointment = appList )
        }.toMono()
    }

    private fun saveMedicare(medicare: Medicare): Mono<UserResponse> {
        val appList = appointmentRepository.findByAppointmentWith(medicare.medicare_id)
        return medicareRepository.save(medicare).map{
            UserResponse(id = medicare.medicare_id , name = medicare.medicare_name, type = Type.MEDICARE, status = Status.LOGIN, group = Type.MEDICARE.name + "_GROUP",appointment = appList )
        }.toMono()
    }

    private fun savePatient(patient: Patient): Mono<UserResponse> {
        val appList = appointmentRepository.findByAppointmentWith(patient.patientId)
        return patientRepository.save(patient).map{
            UserResponse(id = patient.patientId , name = patient.patientName, type = Type.PATIENT, status = Status.LOGIN, group = Type.DOCTOR.name + "_GROUP",appointment = appList )
        }.toMono()
    }
}