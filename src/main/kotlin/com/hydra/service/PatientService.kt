package com.hydra.service

import com.hydra.data.objects.Appointment
import com.hydra.data.objects.Patient
import com.hydra.repository.PatientRepository
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import java.time.Instant
import java.util.*

@Service
class PatientService(private val patientRepository: PatientRepository , private val appointmentService: AppointmentService) {

    fun insertPatient(patient: Patient): Mono<Patient> {
        val appointment = Appointment(
                appointmentId = UUID.randomUUID().toString(),
                appointmentDate = Instant.now(),
                appointmentFrom = patient.patientName,
                appointmentStatus = "Scheduled",
                appointmentWith = patient.patientDoctors[0]
        )

        appointmentService.insertAppointment(appointment)
        return patientRepository.save(patient).single()
    }
    fun fetchPatientById(patientId: String) : Mono<Patient> = patientRepository.findById(patientId).single()
    fun updatePatient(patient: Patient): Mono<Patient> = patientRepository.save(patient).single()
}