package com.hydra.data.objects

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant

@Document(collection = "user")
data class User(
        @Id
        val id: String,
        val name: String,
        val password: String?,
        val type: Type,
        private val date : Instant,
        val status: Status,
        val group: String
)

enum class Status {
    LOGIN, LOGOUT
}

enum class Type {
    DOCTOR , PATIENT , ASSISTANT, MEDICARE
}