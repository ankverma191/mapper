package com.hydra.data.objects

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "doctor")
data class Doctor (
        @Id
        val doctor_id: String,
        val doctor_name: String,
        val doctor_patients: List<String>?,
        val doctor_medicare: List<String>?
)