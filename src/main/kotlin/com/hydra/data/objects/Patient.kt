package com.hydra.data.objects

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "patient")
data class Patient (
        @Id
        val patientId: String,
        val patientName: String?,
        val patientType: PatientType?,
        val patientDoctors: List<String>,
        val patientMedicare: List<String>?
)


enum class PatientType {
        CRITICAL, NORMAL
}