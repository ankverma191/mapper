package com.hydra.data.objects

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant

@Document(collection = "appointment")
data class Appointment (
        @Id
        val appointmentId: String,
        val appointmentDate: Instant,
        val appointmentFrom: String?,
        val appointmentWith: String?,
        val appointmentStatus: String
)