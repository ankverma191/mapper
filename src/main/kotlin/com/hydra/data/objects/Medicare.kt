package com.hydra.data.objects

import org.springframework.data.annotation.Id

data class Medicare (
        @Id
        val medicare_id: String,
        val medicare_name: String,
        val medicare_patients: List<String>?,
        val medicare_doctors: List<String>?
)